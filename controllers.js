const fs = require('fs').promises;
const path = require('path');

const isFileValid = file => {
  const regex = /[a-zA-Z0-9]*\.(log|txt|json|yaml|xml|js)$/g;
  return regex.test(file);
};

const createFile = async (req, res) => {
  try {
    const { filename, content } = req.body;

    if (!content) {
      throw new Error("Please specify 'content' parameter");
    }

    if (!isFileValid(filename)) {
      throw new Error('File format not valid');
    }

    await fs.writeFile(`./api/files/${filename}`, `${content}`, 'utf-8');
    res.status(200).json({
      message: 'File created successfully',
    });
  } catch (error) {
    if (error.message === 'File format not valid') {
      res.status(400).json({
        message: 'Supported file formats: log|txt|json|yaml|xml|js',
      });
    } else if (error.message === "Please specify 'content' parameter") {
      res.status(400).json({
        message: "Please specify 'content' parameter",
      });
    } else {
      res.status(500).json({
        message: 'Server error',
      });
    }
  }
};

const getFiles = async (req, res) => {
  try {
    const files = await fs.readdir('./api/files');
    res.status(200).json({ message: 'Success', files: files });
  } catch (error) {
    if (error.message.includes('no such file or directory')) {
      res.status(500).json({ message: 'Server error' });
    } else {
      res.status(400).json({ message: 'Client error' });
    }
  }
};

const getFile = async (req, res) => {
  const { filename: fileName } = req.params;
  const files = await fs.readdir('./api/files');
  try {
    if (!files.some(filename => filename === fileName)) {
      throw new Error('No such file');
    }

    const content = await fs.readFile(`./api/files/${fileName}`);
    const extension = fileName.split('.').pop();
    const loadDate = (await fs.stat(`./api/files/${fileName}`)).mtime;

    res.status(200).json({
      message: 'Success',
      filename: fileName,
      content: content.toString(),
      extension: extension,
      uploadedDate: loadDate,
    });
  } catch (error) {
    if (error.message === 'No such file') {
      res.status(400).json({
        message: `No file with '${fileName}' filename found`,
      });
    } else {
      res.status(500).json({
        message: 'Server error',
      });
    }
  }
};

module.exports = {
  createFile,
  getFiles,
  getFile,
};
