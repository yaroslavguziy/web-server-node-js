const express = require('express');
const app = express();
const morgan = require('morgan');
const port = 8080;
const controllers = require('./controllers');

app.use(express.json());
app.use(morgan('combined'));

app.post('/api/files', controllers.createFile);

app.get('/api/files', controllers.getFiles);

app.get('/api/files/:filename', controllers.getFile);

app.listen(port, () => console.log(`Server started on port ${port}`));
